import React, { useState, useEffect } from "react";
import { commerce } from "../../lib/commerce";
import { AppBar, Button, Toolbar,Grid } from "@material-ui/core";
import { Link } from "react-router-dom";
import Product from "./Product";
function Products({products,addToCart}) {
  return (
    <div>
      <AppBar
        style={{ display: "flex", justifyContent: "space-between" }}
        color="inherit"
      >
        <Toolbar style={{ display: "flex", justifyContent: "space-between" }}>
          <img
            className="img-logo"
            src="https://crystaldelta.com/wp-content/themes/CrystalDelta/img/logo/crystal-delta_b.png"
            alt=""
          />
          <Button variant="outlined" color="primary">
            Logout
          </Button>
        </Toolbar>
      </AppBar>
      <div style={{ margin: "15vh" }}></div>
      <Link className="link" to="/">
        All Courses
      </Link>
      <Link className="link" to="/dashboard">
        My Dashboard
      </Link>
      <Grid container justify="center" spacing={4}>
        {products.map((product) => (
          <Grid key={product.id} item xs={12} sm={6} md={4} lg={3}>
            <Product key={product.id} product={product} addToCart={addToCart}/>
          </Grid>
        ))}
      </Grid>
    </div>
  );
}

export default Products;
