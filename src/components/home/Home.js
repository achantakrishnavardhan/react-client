import React from 'react'
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import {Typography,Button} from '@material-ui/core'

import Login from './Login';
import Signup from './Signup';
function Home() {
    const[view,setview]=React.useState(false)
    
    return (
       <div>
           <AppBar  color='inherit'>
               <Toolbar className='home-root'>
                   <img className='img-logo' src='https://crystaldelta.com/wp-content/themes/CrystalDelta/img/logo/crystal-delta_b.png' alt=''/>
                   <Typography style={{fontWeight:'bold',fontSize:'30px',textAlign:'center'}}>
                       E-LEARNING
                   </Typography>
                   <div>
                   <Button style={{margin:'10px'}} variant='outlined' color='primary' onClick={()=>{setview(true)}}> Login</Button>
                   <Button  variant='outlined' color='primary' onClick={()=>{setview(false)}}>Sign Up</Button>
                   </div>
               </Toolbar>
         </AppBar>
      {  view? <Login />:
        <Signup/>}
       </div>
    )
}

export default Home
