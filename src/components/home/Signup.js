import React,{useState} from "react";
import {
  Card,
  makeStyles,
  CardContent,
  Typography,
} from "@material-ui/core";
import {auth} from '../firebase'
import theme from "./thems";
const useStyles = makeStyles((theme) => ({
  button: { ...theme.mybutton },
  nav: { ...theme.myNav },
  small: {
    display: "none",
    [theme.breakpoints.up("sm")]: {
      display: "block"
    },
  },
  large: {
    display: "block",
    [theme.breakpoints.up("sm")]: {
      display: "none",
    },
  },
  textarea: {
    marginTop:'10px',
    marginBottom:'10px',
    width:'300px',
    padding:'1vh',
    outline:'none',
    fontSize:'2.5vh'
  },
  text: {
    fontFamily: "Rosarivo",
    fontSize: "50px",
    padding: theme.spacing(3),
    color: "#1B224A",
    textAlign:'center'
  },
  text1: {
    fontFamily: "Rosarivo",
    fontSize: "20px",
    padding: theme.spacing(1),
    color: "#1B224A",
    textAlign:'center'
  },
  root: {
    maxWidth: 445,
    textAlign: "center",
  },
}));
export default function Signup(props) {
  const classes = useStyles();
const[error,setError]=useState('');
const[loading,setLoading]=useState(false);
const [email,setRegisterEmail]=useState('');
const [password,setRegisterPassword]=useState('');
const [confirmPassword,setCofirmRegisterPassword]=useState('');

const [user, setUser] = useState();

onAuthStateChanged(auth, (currentUser) => {
  setUser(currentUser);
});

async function handleSubmit(e) {
  e.preventDefault()
  if (password!== confirmPassword) {
    return setError("Passwords do not match")
  }
  try {
    setError("")
    setLoading(true)
   const user= await createUserWithEmailAndPassword(
     auth,
     email,
     password
   )
   props.setuser(user)
   setUser(user)
  } catch {
    setError("Failed to create an account")
  }

  setLoading(false)
}
  return (
    <div style={{ backgroundColor: "#C7DFC1", height: "90vh" }}>
         <div style={{margin:'11vh'}}>

</div>
      <Typography variant="h1" className={classes.text}>
      Welcome to Crystal Delta e-learning
      </Typography>
      <Card
        className={classes.root}
        style={{ marginLeft: "420px", marginTop: "10px" }}
      >
        <CardContent>
          <form onSubmit={handleSubmit} className={classes.root}>
          <Typography>{error}</Typography>
            <Typography
              gutterBottom
              variant="h5"
              component="h2"
              style={{ textAlign: "center", fontFamily: "Rosarivo" }}
            >
              New User 
            </Typography>
            <input required placeholder="Email Address" className={classes.textarea} type='text' onChange={(event)=>{setRegisterEmail(event.target.value)}}/><br/>
            <input required placeholder="Password"  className={classes.textarea} type='password'  onChange={(event)=>{setRegisterPassword(event.target.value)}}/>
            <br />
            <input required placeholder="Confirm Password"  className={classes.textarea} type='password'  onChange={(event)=>{setCofirmRegisterPassword(event.target.value)}}/>
            <br />
            <button
            disabled={loading}
              type="submit"
              className={classes.button}
              style={{width: "150px",padding:'1vh'}}
            >Sign Up</button>
          </form>
          
        </CardContent>
      </Card>
      <Typography className={classes.text1}  onClick={()=>props.changelog(true)} style={{cursor:'pointer'}}>
              Already have an account? Log In
          </Typography>
    </div>
  );
}
