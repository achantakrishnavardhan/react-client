import "./App.css";
import React from "react";
import Home from "./components/home/Home";
import Products from "./components/products/products";
import Cart from "./components/dashboard/Cart";
import { commerce } from "./lib/commerce";
import { BrowserRouter, Routes, Route } from "react-router-dom";
function App() {
  const [cart, setCart] = React.useState({});
  const [User, setUser] = React.useState({});

  const [products, setProducts] = React.useState([]);
  const fetchdata = async () => {
    const { data } = await commerce.products.list();
    setProducts(data);
  };
  const fetchCart = async () => {
    setCart(await commerce.cart.retrieve());
  };

  React.useEffect(() => {
    fetchdata();
    fetchCart();
  }, []);
  console.log(cart);
  const handleCart = async (productid, quantity) => {
    const item = await commerce.cart.add(productid, quantity);
    setCart(item.cart);
  };
  //add to cart

  const handlequantity = async (productid, quantity) => {
    const { cart } = await commerce.cart.update(productid, { quantity });
    setCart(cart);
  };

  //remove from cart
  const removecart = async (productid) => {
    const response = await commerce.cart.remove(productid);
    setCart(response.cart);
  };

  // empty your cart

  const emptycart = async () => {
    const response = await commerce.cart.empty();
    setCart(response.cart);
  };
  return (
    // <Home/>
    <>
    <BrowserRouter>
    <Routes>
      
      <Route path="/" element={<Products addToCart={handleCart} products={products} setUser={setUser} />} />
      <Route path="/dashboard" element={ <Cart
        cart1={cart}
        handlequantity={handlequantity}
        removecart={removecart}
        emptycart={emptycart}
      />} />
    </Routes>
  </BrowserRouter>
      
     
    </>
  );
}

export default App;
